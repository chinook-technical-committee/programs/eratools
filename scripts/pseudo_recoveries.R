# Created: 2020-12-03 17:33:28






# libraries ---------------------------------------------------------------


rm(list = ls())

require(eratools)


# setup variables ---------------------------------------------------------

curryr <- 2019

#caspathold <-'C:/Users/worc/Documents/CTC/ream/devs/testtxt/CASClient_2019_BE.mdb'
caspath <-'C:/Users/worc/Documents/CTC/ERA/2020/aux_files/pseudo_recs/CASClient_2020_BE.mdb'

caspath <- 'C:/Users/folkesm/Documents/Projects/chinook/exploitationRateAnalysis/analyses/analysisYear2020'
cas.filepath <- list.files(path = caspath, pattern = 'mdb$', full.names = TRUE, recursive = TRUE)


# data import -------------------------------------------------------------

#casdf <- readCASpseudorecdata(caspath=caspath, stock='COW', curryr=2018)
casdf<- readCASpseudorecdata(caspath=cas.filepath[1], stock='COW', curryr=curryr)

#pastcas <-  read.csv('CASCOW.csv')
#note from CW: this is the spreadsheet that includes the release numbers (I got it  from mrp.. I actually probably just got the latest version from Nick, it is probably something that can be easily automated now that you can access MRP from R.) The idea is that the expected number of recoveries in the puget sound fisheries depends on previous recoveries in those fisheries and on the release info for the stock (more releases will result in higher expected recoveries)
pastmrp <-  read.csv(paste(caspath, 'COWTagWireCode.csv', sep = '/'), stringsAsFactors = FALSE)
#pastmrp <- as.data.frame(readxl::read_excel(paste(caspath, 'COW_WDFWpseudorecs_2018_v2.xlsx', sep = '/'), sheet = 'WireTagCode'))
#some numbers come in as strings so convert:
# pastmrp <- type.convert(pastmrp, as.is=TRUE)
 
names(pastmrp)
# names(pastmrp.csv)

str(pastmrp)

names(fishery_grp)


# data update and calculation ---------------------------------------------

addfisherygrouping()
casdf <- addfisherygrouping(casdf=casdf)
calcpseudorec(casdf=casdf$casdf, mrp=pastmrp, stock='COW', curryr=2019)
calcpseudorec(casdf=casdf$casdf, mrp=pastmrp, stock='COW', curryr=2019, psauxname=paste(caspath, 'COWpsdoaux2019.csv', sep = '/'))





