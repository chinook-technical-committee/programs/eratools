

# packages ----------------------------------------------------------------

require(eratools)




# compare cmz files -------------------------------------------------------

cmz2019 <- "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/analyses/analysisYear2019/2019ERA Mortality Distrib Tables/2019ERA Mortality Distrib Tables V3/catchDistribution_CMZ.csv"

cmz2020 <- "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/analyses/analysisYear2020/stockERA/distributionTables/catchDistribution_CMZ.csv"

res <- compareCMZ(cmzpathold = cmz2019, cmzpathnew = cmz2020, stock=c("COW", "QUI"))

# values that need attention (differences >1%)
res$nonzerodiff
res$diffgreaterthan

#compare all stocks (the default):
res <- compareCMZ(cmzpathold = cmz2019, cmzpathnew = cmz2020)
res$diffgreaterthan

#export results to excel:
writeXLlistofDF(res, "compareCMZ.xls")

