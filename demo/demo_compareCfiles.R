

# comments ----------------------------------------------------------------

#this is for comparing c files, usually between era years.
#output can be written to excel, as shown below


# packages ----------------------------------------------------------------

require(eratools)


# setup variables -------------------------------------------------------

path_prioryear <- "C:\\Users\\folkesm\\Documents\\Projects\\salmon\\chinook\\ctc\\awg\\exploitationRateAnalysis\\analyses\\analysisYear2021\\data\\ERA Input Files\\ERA_2021_IN_v3"
path_newyear <- "C:\\Users\\folkesm\\Documents\\Projects\\salmon\\chinook\\ctc\\awg\\exploitationRateAnalysis\\analyses\\analysisYear2022\\data\\ERA_2022_Production Files\\ERA_2022_Production Files\\With 2019 PseudoRecs"



# initial diagnostics  --------------------------------------------------

#identify C filenames:
cfilenames_prioryear <- listCfiles(path_prioryear, recursive = TRUE)

#need to limit depth of c file listing as Antonio has added 2021 folder in each stock folder
dirs <- list.dirs(path_newyear, recursive = FALSE)
dirs <- dirs[-3]
cfilenames_newyear <- lapply(dirs, listCfiles)
cfilenames_newyear <- unlist(cfilenames_newyear)
#cfilenames_newyear <- listCfiles(path_newyear, recursive = TRUE)

#find what stocks are available in new c file set:
stocks <- sort(unique(tools::file_ext(cfilenames_newyear)))

#this shows what c files are in old but not in new
setdiff(basename(cfilenames_prioryear)[tools::file_ext(cfilenames_prioryear) %in% stocks], basename(cfilenames_newyear)[tools::file_ext(cfilenames_newyear) %in% stocks])

#this shows what c files are in new but not in old
setdiff(basename(cfilenames_newyear)[tools::file_ext(cfilenames_newyear) %in% stocks], basename(cfilenames_prioryear)[tools::file_ext(cfilenames_prioryear) %in% stocks] )


#check that there is a c file for each code in the stock specific .cds files:
cfile.missing <- lapply(dirs, function(dir){
#  browser()
  cds.filename <- list.files(dir, "cds$", ignore.case = TRUE, full.names = TRUE)
  data.cds <- readCDSfile(cds.filename)
  compareCDSwithCfiles(cds.df = data.cds, cfile.dir = dir)
  })
names(cfile.missing) <- basename(dirs)
cfile.missing



# import preparation ------------------------------------------------------

#if wanting to subset c files to include only codes in the stock specific .cds files
#this subsets the vector of new c file names to
#cds.filenames <- list.files(path_newyear, "cds$", recursive = TRUE, ignore.case = TRUE, full.names = TRUE)
cds.filenames <- lapply(dirs, function(x) list.files(x, "cds$", recursive = FALSE, ignore.case = TRUE, full.names = TRUE))
data.cds <- unlist(lapply(cds.filenames, function(filename){readCDSfile(filename)[,2]}))

name.code <- tools::file_path_sans_ext(basename(cfilenames_newyear))
name.code <- gsub("C", "", name.code)
#here the vector of c file names is now subsetted:
cfilenames_newyear.sub <- cfilenames_newyear[name.code %in% data.cds]

#this shows that it removed 29 c files as they are not in the cds file:
length(cfilenames_newyear)
length(cfilenames_newyear.sub)

basename(setdiff(cfilenames_newyear, cfilenames_newyear.sub))

# import ------------------------------------------------------------------


#import the c files
res_prioryear <- readCfiles(cfilenames_prioryear)

#choose subsetted new files or not:
res_newyear <- readCfiles(cfilenames_newyear)
res_newyear <- readCfiles(cfilenames_newyear.sub)


# comparison --------------------------------------------------------------

#compare C files, which have been summed by BroodYear
#as argument matchfiles is left at default=TRUE, only matching c files are used
#see ?compareCfiles2 for more information about matchfiles argument
diffs <- compareCfiles2(CList_old = res_prioryear, CList = res_newyear, groupby = "BroodYear")


diffs$differenceNonZero

#export results to excel (only include the tables indicating differences):
writeXLlistofDF(diffs[! names(diffs) %in% c("newCsummary", "oldCsummary", "difference")], "comparec.xls")

#this exports all data frames. Warning as it's a lot. the xls file will be >75megs.
#writeXLlistofDF(diffs, "comparec.xls")


#compare C files, using grouping default, which is by "BroodYear", "TagCode", "stock", "Fisheries":
diffs <- compareCfiles2(CList_old = res_prioryear, CList = res_newyear)
names(diffs)
str(diffs)
tail(diffs$difference)
head(diffs$differenceNonZero)
dim(diffs$differenceNonZero)


#export results to excel:
writeXLlistofDF(diffs$differenceNonZero, "comparec_detailed.xls")


# compare by stock to get individual xls files---------------------------------

stocks <- sort(unique(tools::file_ext(cfilenames_newyear)))

res <- lapply(stocks, function(stock){
  #import the c files
  res_prioryear <- readCfiles(cfilenames_prioryear[tools::file_ext(cfilenames_prioryear)==stock])
  res_newyear <- readCfiles(cfilenames_newyear[tools::file_ext(cfilenames_newyear)==stock])
  
  # res_prioryear <- readCfiles(cfilenames_prioryear.sub[tools::file_ext(cfilenames_prioryear.sub)==stock])
  # res_newyear <- readCfiles(cfilenames_newyear.sub[tools::file_ext(cfilenames_newyear.sub)==stock])
  
  #compare C files, using grouping default, which is by "BroodYear", "TagCode", "Fisheries":
  diffs <- compareCfiles2(CList_old = res_prioryear, CList = res_newyear)
  
  #export results to excel:
  filename <- paste0("comparec_detailed_",stock, ".xls")
  
  writeXLlistofDF(diffs[! names(diffs) %in% c("newCsummary", "oldCsummary", "difference")], filename = filename)
  return(diffs)
})
names(res) <- stocks
