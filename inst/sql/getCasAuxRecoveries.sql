SELECT CWDBRecovery.RecoveryId, 
       CWDBRecovery.RunYear, 
      WireTagCode.Stock, 
      CWDBRecovery.TagCode, 
      CWDBRecovery.CWDBFishery, 
      CWDBRecovery.Fishery, 
      CWDBRecovery.EstimatedNumber,
      CWDBRecovery.Auxiliary
  FROM CWDBRecovery 
  INNER JOIN WireTagCode ON CWDBRecovery.TagCode = WireTagCode.TagCode
WHERE Agency = '{agency_code}' AND CWDBRecovery.Auxiliary = True;