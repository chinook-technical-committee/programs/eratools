Below are rules for rejecting certain recoveries from being imported into the CAS database:

1. All recoveries for which species is not chinook (species = 1; even though the tagcode identifies a recovery to species, those not correctly identified at the time of sampling will not be expanded properly)

2. All recoveries from juvenile and high seas sampling programs (the coded value in the RMIS fishery field starts with 7 or 8, or has a value greater than or equal to 70 and less than 90)

3. All Canadian escapement recoveries (the recovery_location_code starts with 2F and the value in the fishery field equals 50 or 54).  Also all AK escapement recoveries.

4. All AK marine recoveries that did not occur in SEAK.  The recovery_location_code for marine recoveries in SEAK begin with '1M1' with the reporting agency equal to 'ADFG'; these recoveries are incorporated into CAS.  Marine non-SEAK recoveries begin with 1M[2-9] and are excluded from CAS.

5. All AK freshwater recoveries (the reporting_agency equals ADFG and the recovery_location_code starts with '1F')

6. All recoveries where the estimated_number = 0 (please note that there are also many recoveries where the estimated_number field is blank; these are dealt with separately because CAS assigns an 'adjusted estimated' number to some of these)

7. All recoveries where sample_type = 5 (these do not have an expanded number associated with them)


Please Note:  Some sample_type = 4 will be rejected by CAS (the estimated_number field is also blank for these recoveries in RMIS). However, most are retained by CAS and CAS calculates an adjusted estimated number for them.  There may be a rule by which to select those recoveries but I don't know it at this time.  If this rule can be determined, then it could also be employed to screen recoveries before incorporation into CAS.

Also note any recoveries that have an out-of range total age (i.e., <0 or >8).

Canadian recoveries starting with the inital characters as given in the following list are always excluded from CAS.  These are historical cases of cross-border samples (catch was taken in one country but landed & sampled in another) and are enerally without an associated catch estimate.  They're few in number and not to be worried about.

2MN07
2MS03
2M*47
2M*60
