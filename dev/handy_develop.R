#=========================================
#this file contains a bunch of stuff on building packages and on testing the functions

devtools::document()

#equivalent to ctrl + b in Rstudio 
#devtools::build(binary= TRUE)
devtools::build()


?compareCMZ
#try installing from git
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/eratools")


#testing of compareCMZ


 cmzcomp <- compareCMZ(cmzpathnew="C:/Users/worc/Documents/CTC/ERA/2020/mortality_distribution_table/result/catchDistribution_CMZ.csv",
	cmzpathold="C:/Users/worc/Documents/CTC/ERA/2019/mortality_distribution_tables/mortdist/catchDistribution_CMZ.csv",
	stock=c("BQR", "CHI", "COW", "QUI"))

names( cmzcomp)
cmzcomp$nonzerodiff
cmzcomp$diffgreatone


