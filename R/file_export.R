




#' Write CM1 list to CM1 file
#'
#' @param cm1list 
#'
#' @return
#' @export
#'
#' @examples
writeCM1 <- function(cm1list, overwrite=FALSE){
  
  lapply(cm1list, function(x){
      dat.tmp <- lapply(x$data, function(x){
      x$value <- formatC(x$value, width = 11, flag="-")
      paste(unlist(x), collapse = ", ")
    })
    dat.tmp <- do.call(rbind, dat.tmp)
    if((file.exists(x$filepath) & overwrite==TRUE) | (! file.exists(x$filepath)  )) {
      write(x = dat.tmp, file = x$filepath)
      write("", x$filepath, append = TRUE)
      write(x$notes, x$filepath, append = TRUE)  
    }else{
      cat(paste("\nThis file was not overwritten:\n", x$filepath))
    }
    
  })
  
  
}#END writeCM1




#' Export CM1 list to csv file
#'
#' @param cm1list 
#' @param include.description 
#' @param filename 
#'
#' @return
#' @export
#'
#' @examples
writeCM1toCSV <- function(cm1list, include.description=FALSE, filename="cm1_master.csv"){
  
  dat.list <- lapply(cm1list, function(x){
    
    dat.tmp <- lapply(x$data, "[[", "value")
    dat.tmp <- do.call(cbind, dat.tmp)
    
    if(include.description){
      dat.desc <- lapply(x$data, "[[", "description")
      dat.desc <- do.call(cbind, dat.desc)
      colnames(dat.desc) <- paste0(colnames(dat.desc), "_desc")
      dat.tmp <- cbind(dat.tmp, dat.desc)
    }
    dat.tmp
  })
  
  dat.df <- do.call(rbind, dat.list)
  
  write.csv(dat.df, filename, row.names = FALSE, quote = FALSE)
  
}#END writeCM1toCSV
